package view;

import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;
import java.awt.GridBagLayout;
import java.awt.GridBagConstraints;
import java.awt.Insets;
import java.awt.Toolkit;
import java.awt.Font;
import javax.swing.border.TitledBorder;
import java.awt.Color;
import java.awt.Dimension;

import javax.swing.border.LineBorder;

public class VistaSitio extends JFrame{
	private static VistaSitio INSTANCE;
	
	private JPanel contentPane;
	
	private JTextField txtServicio;
	private JTextField txtZona;
	
	private JButton btnEnviarConsulta;
	
	private JLabel lblServicio;
	private JLabel lblZona;
	private JPanel panelBotones;
	private JPanel panelFormulario;
	private JLabel lblImagen;
	
	private VistaSitio() {
		super();
		setTitle("MatchMaker - Consulta");

		setFont(new Font("Century Gothic", Font.PLAIN, 12));
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
		setBounds((int) (screenSize.getWidth()/2 - 200), (int) (screenSize.getHeight()/2 - 100), 400, 221);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		GridBagLayout gbl_contentPane = new GridBagLayout();
		gbl_contentPane.columnWidths = new int[]{369, 0};
		gbl_contentPane.rowHeights = new int[]{152, 0};
		gbl_contentPane.columnWeights = new double[]{1.0, Double.MIN_VALUE};
		gbl_contentPane.rowWeights = new double[]{1.0, Double.MIN_VALUE};
		contentPane.setLayout(gbl_contentPane);
		
		JPanel panelConsulta = new JPanel();
		panelConsulta.setBorder(null);
		GridBagConstraints gbc_panelConsulta = new GridBagConstraints();
		gbc_panelConsulta.fill = GridBagConstraints.BOTH;
		gbc_panelConsulta.gridx = 0;
		gbc_panelConsulta.gridy = 0;
		contentPane.add(panelConsulta, gbc_panelConsulta);
		GridBagLayout gbl_panelConsulta = new GridBagLayout();
		gbl_panelConsulta.columnWidths = new int[]{0, 135, 0};
		gbl_panelConsulta.rowHeights = new int[]{81, 0, 0};
		gbl_panelConsulta.columnWeights = new double[]{0.0, 1.0, Double.MIN_VALUE};
		gbl_panelConsulta.rowWeights = new double[]{1.0, 0.0, Double.MIN_VALUE};
		panelConsulta.setLayout(gbl_panelConsulta);
		
		Icon imgIcon = new ImageIcon((System.getProperty("user.dir")+"/resources/img.png"));
		lblImagen = new JLabel(imgIcon);
		GridBagConstraints gbc_lblImagen = new GridBagConstraints();
		gbc_lblImagen.insets = new Insets(0, 0, 5, 5);
		gbc_lblImagen.gridx = 0;
		gbc_lblImagen.gridy = 0;
		panelConsulta.add(lblImagen, gbc_lblImagen);
		
		panelFormulario = new JPanel();
		panelFormulario.setBorder(new LineBorder(new Color(255, 165, 0), 1, true));
		GridBagConstraints gbc_panelFormulario = new GridBagConstraints();
		gbc_panelFormulario.insets = new Insets(5, 5, 5, 0);
		gbc_panelFormulario.fill = GridBagConstraints.BOTH;
		gbc_panelFormulario.gridx = 1;
		gbc_panelFormulario.gridy = 0;
		panelConsulta.add(panelFormulario, gbc_panelFormulario);
		GridBagLayout gbl_panelFormulario = new GridBagLayout();
		gbl_panelFormulario.columnWidths = new int[]{0, 0, 0};
		gbl_panelFormulario.rowHeights = new int[]{0, 0, 0};
		gbl_panelFormulario.columnWeights = new double[]{1.0, 1.0, Double.MIN_VALUE};
		gbl_panelFormulario.rowWeights = new double[]{0.0, 0.0, Double.MIN_VALUE};
		panelFormulario.setLayout(gbl_panelFormulario);
		
		lblServicio = new JLabel("Servicio:");
		lblServicio.setBackground(new Color(255, 165, 0));
		GridBagConstraints gbc_lblServicio = new GridBagConstraints();
		gbc_lblServicio.anchor = GridBagConstraints.WEST;
		gbc_lblServicio.insets = new Insets(5, 5, 5, 5);
		gbc_lblServicio.gridx = 0;
		gbc_lblServicio.gridy = 0;
		panelFormulario.add(lblServicio, gbc_lblServicio);
		lblServicio.setFont(new Font("Century Gothic", Font.PLAIN, 12));
		
		txtServicio = new JTextField();
		GridBagConstraints gbc_txtServicio = new GridBagConstraints();
		gbc_txtServicio.fill = GridBagConstraints.HORIZONTAL;
		gbc_txtServicio.insets = new Insets(5, 5, 5, 5);
		gbc_txtServicio.gridx = 1;
		gbc_txtServicio.gridy = 0;
		panelFormulario.add(txtServicio, gbc_txtServicio);
		txtServicio.setFont(new Font("Century Gothic", Font.PLAIN, 12));
		txtServicio.setColumns(10);
		
		lblZona = new JLabel("Zona:");
		lblZona.setBackground(new Color(255, 165, 0));
		GridBagConstraints gbc_lblZona = new GridBagConstraints();
		gbc_lblZona.anchor = GridBagConstraints.WEST;
		gbc_lblZona.insets = new Insets(5, 5, 5, 5);
		gbc_lblZona.gridx = 0;
		gbc_lblZona.gridy = 1;
		panelFormulario.add(lblZona, gbc_lblZona);
		lblZona.setFont(new Font("Century Gothic", Font.PLAIN, 12));
		
		txtZona = new JTextField();
		GridBagConstraints gbc_txtZona = new GridBagConstraints();
		gbc_txtZona.insets = new Insets(5, 5, 5, 5);
		gbc_txtZona.fill = GridBagConstraints.HORIZONTAL;
		gbc_txtZona.gridx = 1;
		gbc_txtZona.gridy = 1;
		panelFormulario.add(txtZona, gbc_txtZona);
		txtZona.setFont(new Font("Century Gothic", Font.PLAIN, 12));
		txtZona.setColumns(10);
		
		panelBotones = new JPanel();
		panelBotones.setBorder(new LineBorder(new Color(255, 165, 0), 1, true));
		GridBagConstraints gbc_panelBotones = new GridBagConstraints();
		gbc_panelBotones.gridwidth = 2;
		gbc_panelBotones.insets = new Insets(5, 5, 0, 0);
		gbc_panelBotones.fill = GridBagConstraints.BOTH;
		gbc_panelBotones.gridx = 0;
		gbc_panelBotones.gridy = 1;
		panelConsulta.add(panelBotones, gbc_panelBotones);
		GridBagLayout gbl_panelBotones = new GridBagLayout();
		gbl_panelBotones.columnWidths = new int[]{87, 0};
		gbl_panelBotones.rowHeights = new int[]{25, 0};
		gbl_panelBotones.columnWeights = new double[]{1.0, Double.MIN_VALUE};
		gbl_panelBotones.rowWeights = new double[]{0.0, Double.MIN_VALUE};
		panelBotones.setLayout(gbl_panelBotones);
		
		btnEnviarConsulta = new JButton("Consultar");
		btnEnviarConsulta.setBackground(new Color(255, 165, 0));
		GridBagConstraints gbc_btnEnviarConsulta = new GridBagConstraints();
		gbc_btnEnviarConsulta.insets = new Insets(5, 5, 5, 5);
		gbc_btnEnviarConsulta.anchor = GridBagConstraints.NORTH;
		gbc_btnEnviarConsulta.gridx = 0;
		gbc_btnEnviarConsulta.gridy = 0;
		panelBotones.add(btnEnviarConsulta, gbc_btnEnviarConsulta);
		btnEnviarConsulta.setFont(new Font("Century Gothic", Font.PLAIN, 12));
		
		this.setVisible(false);
	}
	
	public static VistaSitio getInstance(){
		if(INSTANCE == null){
			INSTANCE = new VistaSitio(); 	
			return INSTANCE;
		}
		else
			return INSTANCE;
	}

	public void mostrarVentana() {
		this.setVisible(true);
	}
	
	public void ocultarVentana() {
		this.setVisible(false);
	}

	public JTextField getTxtServicio() {
		return txtServicio;
	}

	public void setTxtServicio(JTextField txtServicio) {
		this.txtServicio = txtServicio;
	}

	public JTextField getTxtZona() {
		return txtZona;
	}

	public void setTxtZona(JTextField txtZona) {
		this.txtZona = txtZona;
	}

	public JButton getBtnEnviarConsulta() {
		return btnEnviarConsulta;
	}

	public void setBtnEnviarConsulta(JButton btnEnviarConsulta) {
		this.btnEnviarConsulta = btnEnviarConsulta;
	}
	
	public void mostrarPopUpError() {
		JOptionPane.showMessageDialog(
	             null, "Lo sentimos, se ha producido un error.", 
	             "Error", JOptionPane.ERROR_MESSAGE);
	}
}
