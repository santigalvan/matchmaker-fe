package view;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JTable;

import java.awt.GridBagLayout;
import java.awt.GridBagConstraints;
import java.awt.Insets;
import java.util.List;
import java.util.Observable;
import java.util.Observer;

import javax.swing.JLabel;
import javax.swing.border.LineBorder;

import java.awt.Color;

import javax.swing.JButton;
import javax.swing.table.DefaultTableModel;

//import DTO.ServicioDTO;
import controller.MainViewController;
//import model.Model;

public class MainView implements Observer {

	private MainViewController mainViewController;
	//private Model model;
	
	private DefaultTableModel modelServicios;
	private String[] nombreColumnas = { "Nombre", "Categoria", "Descripcion"};
	private JFrame frame;
	private JTable table;

	public MainView() {
		//this.model = model;
		//model.addObserver(this);
		mainViewController = new MainViewController(this);
		initialize();
		//cargarTablaVista();
		this.frame.setVisible(true);
		this.frame.repaint();
	}
/*
	private void cargarTablaVista() {
		List<ServicioDTO> servicios = model.getAllServicios();
		for(ServicioDTO serTemp: servicios) {
			Object[] fila = {serTemp.getNombre(), serTemp.getCategoria(), serTemp.getDescripcion()};
			System.out.println(serTemp.getNombre()+", "+serTemp.getCategoria()+", "+ serTemp.getDescripcion());
			this.modelServicios.addRow(fila);
		}
	}
*/
	private void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 889, 614);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		GridBagLayout gridBagLayout = new GridBagLayout();
		gridBagLayout.columnWidths = new int[]{873, 0};
		gridBagLayout.rowHeights = new int[]{10, 0, 0, 0};
		gridBagLayout.columnWeights = new double[]{1.0, Double.MIN_VALUE};
		gridBagLayout.rowWeights = new double[]{0.0, 1.0, 0.0, Double.MIN_VALUE};
		frame.getContentPane().setLayout(gridBagLayout);
		
		JPanel header = new JPanel();
		header.setBorder(new LineBorder(new Color(65, 105, 225)));
		GridBagConstraints gbc_header = new GridBagConstraints();
		gbc_header.insets = new Insets(5, 5, 5, 0);
		gbc_header.fill = GridBagConstraints.BOTH;
		gbc_header.gridx = 0;
		gbc_header.gridy = 0;
		frame.getContentPane().add(header, gbc_header);
		
		JLabel lblTitulo = new JLabel("Titulo");
		header.add(lblTitulo);
		
		JPanel body = new JPanel();
		GridBagConstraints gbc_panel = new GridBagConstraints();
		gbc_panel.insets = new Insets(0, 0, 5, 0);
		gbc_panel.fill = GridBagConstraints.BOTH;
		gbc_panel.gridx = 0;
		gbc_panel.gridy = 1;
		frame.getContentPane().add(body, gbc_panel);
		
		table = new JTable();
		body.add(table);
		
		JPanel footer = new JPanel();
		GridBagConstraints gbc_footer = new GridBagConstraints();
		gbc_footer.insets = new Insets(5, 5, 0, 0);
		gbc_footer.fill = GridBagConstraints.BOTH;
		gbc_footer.gridx = 0;
		gbc_footer.gridy = 2;
		frame.getContentPane().add(footer, gbc_footer);
		
		JButton btnAceptar = new JButton("Aceptar");
		footer.add(btnAceptar);
	}

	@Override
	public void update(Observable o, Object arg) {
		
	}
	/*
	public Model getModel() {
		return model;
	}
	*/
}
