package view;

import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;
import java.awt.GridBagLayout;
import java.awt.GridBagConstraints;
import java.awt.Insets;
import java.awt.Toolkit;

import javax.swing.UIManager;
import java.awt.Color;
import java.awt.Dimension;

import javax.swing.border.LineBorder;
import java.awt.Font;

public class Vista {
	private JFrame frmMatchmakerListado;
	private JTable tablaResultados;
	
	private DefaultTableModel modelSitio;
	private String[] nombreColumnas = {"Descripción"};
	private JPanel panelBotones;
	private JButton btnConsultaSitio;
	
	
	public Vista() {
		super();
		inicializar();
	}
	
	private void inicializar() {
		frmMatchmakerListado = new JFrame();
		frmMatchmakerListado.setFont(new Font("Century Gothic", Font.PLAIN, 12));
		frmMatchmakerListado.getContentPane().setFont(new Font("Century Gothic", Font.PLAIN, 12));
		frmMatchmakerListado.setTitle("MatchMaker - Listado");
		Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
		frmMatchmakerListado.setBounds((int) (screenSize.getWidth()/2 - 500), (int) (screenSize.getHeight()/2 - 225), 1000, 450);
		//frmMatchmakerListado.setBounds(100, 100, 971, 450);
		frmMatchmakerListado.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		modelSitio = new DefaultTableModel(null,nombreColumnas);
		GridBagLayout gridBagLayout = new GridBagLayout();
		gridBagLayout.columnWidths = new int[]{1115, 0};
		gridBagLayout.rowHeights = new int[]{332, 11, 0};
		gridBagLayout.columnWeights = new double[]{1.0, Double.MIN_VALUE};
		gridBagLayout.rowWeights = new double[]{1.0, 0.0, Double.MIN_VALUE};
		frmMatchmakerListado.getContentPane().setLayout(gridBagLayout);
				
				JPanel panelTablaResultado = new JPanel();
				panelTablaResultado.setBackground(UIManager.getColor("CheckBox.light"));
				panelTablaResultado.setBorder(new LineBorder(new Color(255, 165, 0), 1, true));
				GridBagConstraints gbc_panelTablaResultado = new GridBagConstraints();
				gbc_panelTablaResultado.fill = GridBagConstraints.BOTH;
				gbc_panelTablaResultado.insets = new Insets(5, 5, 5, 5);
				gbc_panelTablaResultado.gridx = 0;
				gbc_panelTablaResultado.gridy = 0;
				frmMatchmakerListado.getContentPane().add(panelTablaResultado, gbc_panelTablaResultado);
				GridBagLayout gbl_panelTablaResultado = new GridBagLayout();
				gbl_panelTablaResultado.columnWidths = new int[]{885, 0};
				gbl_panelTablaResultado.rowHeights = new int[]{177, 0};
				gbl_panelTablaResultado.columnWeights = new double[]{1.0, Double.MIN_VALUE};
				gbl_panelTablaResultado.rowWeights = new double[]{1.0, Double.MIN_VALUE};
				panelTablaResultado.setLayout(gbl_panelTablaResultado);
						
						JScrollPane spResultados = new JScrollPane();
						GridBagConstraints gbc_spResultados = new GridBagConstraints();
						gbc_spResultados.insets = new Insets(5, 5, 5, 5);
						gbc_spResultados.fill = GridBagConstraints.BOTH;
						gbc_spResultados.gridx = 0;
						gbc_spResultados.gridy = 0;
						panelTablaResultado.add(spResultados, gbc_spResultados);
						tablaResultados = new JTable(modelSitio);
						tablaResultados.setFont(new Font("Century Gothic", Font.PLAIN, 12));
						
						tablaResultados.getColumnModel().getColumn(0).setPreferredWidth(200);
						tablaResultados.getColumnModel().getColumn(0).setResizable(true);
						
								spResultados.setViewportView(tablaResultados);
				
				panelBotones = new JPanel();
				panelBotones.setBackground(UIManager.getColor("CheckBox.light"));
				panelBotones.setBorder(new LineBorder(new Color(255, 165, 0), 1, true));
				GridBagConstraints gbc_panelBotones = new GridBagConstraints();
				gbc_panelBotones.insets = new Insets(5, 5, 5, 5);
				gbc_panelBotones.fill = GridBagConstraints.BOTH;
				gbc_panelBotones.gridx = 0;
				gbc_panelBotones.gridy = 1;
				frmMatchmakerListado.getContentPane().add(panelBotones, gbc_panelBotones);
				GridBagLayout gbl_panelBotones = new GridBagLayout();
				gbl_panelBotones.columnWidths = new int[]{75, 0};
				gbl_panelBotones.rowHeights = new int[]{23, 0};
				gbl_panelBotones.columnWeights = new double[]{1.0, Double.MIN_VALUE};
				gbl_panelBotones.rowWeights = new double[]{0.0, Double.MIN_VALUE};
				panelBotones.setLayout(gbl_panelBotones);
				
				btnConsultaSitio = new JButton("Consulta");
				btnConsultaSitio.setFont(new Font("Century Gothic", Font.PLAIN, 12));
				GridBagConstraints gbc_btnConsultaSitio = new GridBagConstraints();
				gbc_btnConsultaSitio.fill = GridBagConstraints.VERTICAL;
				gbc_btnConsultaSitio.insets = new Insets(5, 5, 5, 5);
				gbc_btnConsultaSitio.gridx = 0;
				gbc_btnConsultaSitio.gridy = 0;
				panelBotones.add(btnConsultaSitio, gbc_btnConsultaSitio);

	}
	
	public void show() {
		this.frmMatchmakerListado.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
		this.frmMatchmakerListado.addWindowListener(new WindowAdapter() 
		{
			@Override
		    public void windowClosing(WindowEvent e) {
		        int confirm = JOptionPane.showOptionDialog(
		             null, "Estas seguro de querer salir de matchmaker?", 
		             "Salir de MatchMaker", JOptionPane.YES_NO_OPTION,
		             JOptionPane.QUESTION_MESSAGE, null, null, null);
		        if (confirm == 0) {
		        
		           System.exit(0);
		        }
		    }
		});
		this.frmMatchmakerListado.setVisible(true);

	}
	
	public JButton getBtnConsultaSitio() {
		return btnConsultaSitio;
	}

	public DefaultTableModel getModelRespuesta() {
		return modelSitio;
	}
}
