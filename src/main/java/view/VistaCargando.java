package view;

import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRootPane;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;
import java.awt.GridBagLayout;
import java.awt.GridBagConstraints;
import java.awt.Insets;
import java.awt.Toolkit;
import java.awt.Font;
import javax.swing.border.TitledBorder;
import java.awt.Color;
import java.awt.Dimension;

import javax.swing.border.LineBorder;

public class VistaCargando extends JFrame{
	private static VistaCargando INSTANCE;
	
	private JPanel contentPane;
	
	private VistaCargando() {
		super();
		setTitle("Cargando..");

		
		setUndecorated(true);
		getRootPane().setWindowDecorationStyle(JRootPane.NONE);
		setFont(new Font("Century Gothic", Font.PLAIN, 12));
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
		setBounds((int) (screenSize.getWidth()/2 - 150), (int) (screenSize.getHeight()/2 - 150), 300, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		GridBagLayout gbl_contentPane = new GridBagLayout();
		gbl_contentPane.columnWidths = new int[]{369, 0};
		gbl_contentPane.rowHeights = new int[]{152, 0};
		gbl_contentPane.columnWeights = new double[]{1.0, Double.MIN_VALUE};
		gbl_contentPane.rowWeights = new double[]{1.0, Double.MIN_VALUE};
		contentPane.setLayout(gbl_contentPane);
		
		JPanel panelConsulta = new JPanel();
		panelConsulta.setBorder(null);
		GridBagConstraints gbc_panelConsulta = new GridBagConstraints();
		gbc_panelConsulta.fill = GridBagConstraints.BOTH;
		gbc_panelConsulta.gridx = 0;
		gbc_panelConsulta.gridy = 0;
		contentPane.add(panelConsulta, gbc_panelConsulta);
		GridBagLayout gbl_panelConsulta = new GridBagLayout();
		gbl_panelConsulta.columnWidths = new int[]{135, 0};
		gbl_panelConsulta.rowHeights = new int[]{81, 0};
		gbl_panelConsulta.columnWeights = new double[]{1.0, Double.MIN_VALUE};
		gbl_panelConsulta.rowWeights = new double[]{1.0, Double.MIN_VALUE};
		panelConsulta.setLayout(gbl_panelConsulta);
		
		Icon imgIcon = new ImageIcon((System.getProperty("user.dir")+"/resources/loading.gif"));
		JLabel label = new JLabel(imgIcon);
		label.setBounds(668, 43, 46, 14); // for example, you can use your own values
		panelConsulta.add(label);
		
		this.setVisible(false);
	}
	
	public static VistaCargando getInstance(){
		if(INSTANCE == null){
			INSTANCE = new VistaCargando(); 	
			return INSTANCE;
		}
		else
			return INSTANCE;
	}

	public void mostrarVentana() {
		this.setVisible(true);
	}
	
	public void ocultarVentana() {
		this.setVisible(false);
	}

}
