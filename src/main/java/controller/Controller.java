package controller;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;

import dto.ConsultaDTO;
import dto.RespuestaDTO;
import services.ServiceLayer;
import view.VistaSitio;
import view.Vista;
import view.VistaCargando;

public class Controller implements ActionListener, Runnable {
	
	private Vista vistaPrincipal;
	private VistaSitio vistaSitio;
	private VistaCargando vistaCargando;
	private List<RespuestaDTO> respuestas;
	private ConsultaDTO consulta;
	private ServiceLayer sitioService;
	private Thread t;
	
	public Controller(Vista vista, ServiceLayer service) {
		t = new Thread(this);
		this.vistaPrincipal = vista;
		this.sitioService = service;
		this.vistaPrincipal.getBtnConsultaSitio().addActionListener(a->ventanaConsultarSitio(a));
		
		this.vistaSitio = VistaSitio.getInstance();
		this.vistaSitio.getBtnEnviarConsulta().addActionListener(o->enviarConsulta(o));
		
		this.vistaCargando = VistaCargando.getInstance();
	}
	
	private void enviarConsulta(ActionEvent o) {
		String zona = this.vistaSitio.getTxtZona().getText();
		String servicio = this.vistaSitio.getTxtServicio().getText();
		consulta = new ConsultaDTO(servicio, zona);
		this.vistaCargando.mostrarVentana();
		
		t.start();

		this.vistaSitio.ocultarVentana();
	}
	
	private void cargarRespuestasEnTabla(List<RespuestaDTO> respuestas) {
		for (RespuestaDTO rta : respuestas) {
			Object[] fila = { rta.getDescripcion() };
			this.vistaPrincipal.getModelRespuesta().addRow(fila);
		}
	}


	private void ventanaConsultarSitio(ActionEvent a) {
		this.vistaSitio.mostrarVentana();
	}


	@Override
	public void actionPerformed(ActionEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	public void inicializar() {
		this.vistaPrincipal.show();
	}

	@Override
	public void run() {
		this.respuestas = sitioService.find(consulta);
		
		if(respuestas == null) {
			this.vistaSitio.mostrarPopUpError();
		}
		else if(!respuestas.isEmpty()) {
			cargarRespuestasEnTabla(this.respuestas);
		}
		
		this.vistaCargando.ocultarVentana();
	}

}
