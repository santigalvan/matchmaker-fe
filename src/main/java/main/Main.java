package main;

import controller.Controller;
import services.ServiceLayer;
import view.Vista;

public class Main {

	public static void main(String[] args) {
		//Model model = new Model();
		//MainView mainView = new MainView(model);
		
		Vista vista = new Vista();
		ServiceLayer service = new ServiceLayer();
		Controller controlador = new Controller(vista, service);
		controlador.inicializar();
	}

}
